module "project-services" {
  source = "terraform-google-modules/project-factory/google//modules/project_services"

  project_id = var.project_id

  activate_apis = [
    "compute.googleapis.com",
    "iam.googleapis.com",
    "anthos.googleapis.com",
    "container.googleapis.com",
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "cloudtrace.googleapis.com",
    "meshca.googleapis.com",
    "meshtelemetry.googleapis.com",
    "meshconfig.googleapis.com",
    "iamcredentials.googleapis.com",
    "gkeconnect.googleapis.com",
    "gkehub.googleapis.com",
    "multiclusteringress.googleapis.com",
    "multiclusterservicediscovery.googleapis.com",
    "stackdriver.googleapis.com",
    "trafficdirector.googleapis.com",
    "anthosconfigmanagement.googleapis.com",
    "cloudresourcemanager.googleapis.com"
  ]
}

module "vpc" {
  source       = "terraform-google-modules/network/google"
  project_id   = module.project-services.project_id
  network_name = var.network_name
  routing_mode = "GLOBAL"

  subnets = concat([
    for fleet in var.fleets : {
      subnet_name   = fleet.subnet.name
      subnet_ip     = cidrsubnet(fleet.subnet.cidr, 2, 2)
      subnet_region = fleet.region
    }
    ], [
    {
      subnet_name   = var.gke_config.subnet.name
      subnet_ip     = var.gke_config.subnet.ip_range
      subnet_region = var.gke_config.region
    }
  ])

  secondary_ranges = merge({
    for fleet in var.fleets :
    fleet.subnet.name => concat(
      [
        for num in range(fleet.num_clusters) : {
          range_name    = "${fleet.subnet.name}-svc-cidr-${num}"
          ip_cidr_range = cidrsubnet(fleet.subnet.cidr, 7, num + 96)
      }],
      [{
        range_name    = "${fleet.subnet.name}-pod-cidr"
        ip_cidr_range = cidrsubnet(fleet.subnet.cidr, 1, 0)
    }])
    }, {
    "${var.gke_config.subnet.name}" = [
      {
        range_name    = var.gke_config.subnet.ip_range_pods_name
        ip_cidr_range = var.gke_config.subnet.ip_range_pods
      },
      {
        range_name    = var.gke_config.subnet.ip_range_svcs_name
        ip_cidr_range = var.gke_config.subnet.ip_range_svcs
      }
    ]
  })

  firewall_rules = [{
    name        = "allow-all-10"
    description = "Allow Pod to Pod connectivity"
    direction   = "INGRESS"
    ranges      = ["10.0.0.0/8"]
    allow = [{
      protocol = "tcp"
      ports    = ["0-65535"]
    }]
  }]

}

data "google_compute_zones" "available" {
  for_each = toset([for fleet in var.fleets : fleet.region])
  project  = var.project_id
  region   = each.value
}

data "google_project" "project" {
  project_id = var.project_id
}

resource "random_pet" "gke" {
  for_each = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  keepers = {
    gke = each.key
  }
}

locals {
  subnets = module.vpc.subnets
  zones   = data.google_compute_zones.available
  gke_clusters = flatten([[
    for fleet in var.fleets : [
      for num in range(fleet.num_clusters) : {
        zone              = local.zones[fleet.region].names[num % length(local.zones[fleet.region].names)]
        env               = fleet.env
        region            = fleet.region
        subnetwork        = local.subnets["${fleet.region}/${fleet.region}"].name
        ip_range_pods     = "${fleet.region}-pod-cidr"
        ip_range_services = "${fleet.region}-svc-cidr-${num}"
        network           = module.vpc.network_name
        cluster_num       = "gke-${fleet.region}-${num}"
        name              = ""
      }
    ]
    ], [
    {
      zone              = var.gke_config.zone
      env               = var.gke_config.env
      region            = var.gke_config.region
      subnetwork        = var.gke_config.subnet.name
      ip_range_pods     = var.gke_config.subnet.ip_range_pods_name
      ip_range_services = var.gke_config.subnet.ip_range_svcs_name
      network           = module.vpc.network_name
      cluster_num       = var.gke_config.name
      name              = var.gke_config.name
    }
    ]
  ])
}

module "gke" {
  source                    = "terraform-google-modules/kubernetes-engine/google"
  for_each                  = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  project_id                = module.vpc.project_id
  name                      = each.value.name != "" ? "${each.value.name}-${random_pet.gke[each.key].id}" : "gke-${each.value.zone}-${random_pet.gke[each.key].id}"
  regional                  = false
  region                    = each.value.region
  zones                     = [each.value.zone]
  release_channel           = "UNSPECIFIED"
  maintenance_start_time    = "08:00"
  network                   = each.value.network
  subnetwork                = each.value.subnetwork
  ip_range_pods             = each.value.ip_range_pods
  ip_range_services         = each.value.ip_range_services
  default_max_pods_per_node = 64
  network_policy            = true
  cluster_resource_labels   = { "mesh_id" : "proj-${data.google_project.project.number}", "env" : "${each.value.env}", "infra" : "gcp" }
  node_pools = [
    {
      name         = "node-pool-01"
      autoscaling  = true
      auto_upgrade = false
      min_count    = 1
      max_count    = 5
      node_count   = 2
      machine_type = "e2-standard-4"
    },
  ]
}

resource "null_resource" "exec_gateway_apis" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/scripts/gateway_apis.sh"
    environment = {
      VER      = var.gateway_apis_ver
      CLUSTER  = module.gke[var.gke_config.name].name
      LOCATION = module.gke[var.gke_config.name].location
      PROJECT  = var.project_id
    }
  }
  triggers = {
    build_number = "${timestamp()}"
    script_sha1  = sha1(file("${path.module}/scripts/gateway_apis.sh")),
  }
}

resource "null_resource" "exec_mesh" {
  for_each = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/scripts/mesh.sh"
    environment = {
      CLUSTER    = module.gke[each.key].name
      LOCATION   = module.gke[each.key].location
      PROJECT    = var.project_id
      KUBECONFIG = "~/${module.gke[each.key].name}-kubeconfig"
    }
  }
  triggers = {
    build_number = "${timestamp()}"
    script_sha1  = sha1(file("${path.module}/scripts/mesh.sh")),
  }
  depends_on = [module.gke]
}

module "install-asm-managed" {
  source                  = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  for_each                = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  project_id              = var.project_id
  cluster_name            = module.gke[each.key].name
  cluster_location        = module.gke[each.key].location
  kubectl_create_command  = "kubectl apply -f ${path.module}/manifests/asm-managed.yaml && sleep 5 && kubectl wait --for=condition=ProvisioningFinished controlplanerevision asm-managed -n istio-system --timeout 600s"
  kubectl_destroy_command = "kubectl delete -f ${path.module}/manifests/asm-managed.yaml"
  create_cmd_triggers = {
    script_sha1 = sha1(file("${path.module}/manifests/asm-managed.yaml")),
  }
  module_depends_on = [null_resource.exec_mesh]
}

resource "null_resource" "exec_multicluster_secrets" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/scripts/multicluster_secrets.sh"
    environment = {
      CLUSTERS    = join(",", [for cluster in module.gke : cluster.name])
      LOCATIONS   = join(",", [for cluster in module.gke : cluster.location])
      ASM_VERSION = var.asm_version
      PROJECT     = var.project_id
    }
  }
  triggers = {
    build_number = "${timestamp()}"
    script_sha1  = sha1(file("${path.module}/scripts/multicluster_secrets.sh")),
  }
  depends_on = [module.install-asm-managed]
}

resource "null_resource" "exec_test_multicluster" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/scripts/test_multicluster.sh"
    environment = {
      CLUSTERS    = join(",", [for cluster in module.gke : cluster.name])
      LOCATIONS   = join(",", [for cluster in module.gke : cluster.location])
      ASM_VERSION = var.asm_version
      ASM_LABEL   = var.asm_label
      PROJECT     = var.project_id
    }
  }
  triggers = {
    build_number = "${timestamp()}"
    script_sha1  = sha1(file("${path.module}/scripts/test_multicluster.sh")),
  }
  depends_on = [null_resource.exec_multicluster_secrets]
}

resource "google_gke_hub_membership" "membership" {
  for_each      = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  membership_id = each.key
  endpoint {
    gke_cluster {
      resource_link = "//container.googleapis.com/${module.gke[each.key].cluster_id}"
    }
  }
  provider   = google-beta
  depends_on = [module.gke]
}

resource "google_gke_hub_feature" "acm_feature" {
  name       = "configmanagement"
  location   = "global"
  provider   = google-beta
  depends_on = [module.gke]
}

resource "google_gke_hub_feature" "mci_feature" {
  name     = "multiclusteringress"
  location = "global"
  spec {
    multiclusteringress {
      config_membership = google_gke_hub_membership.membership[var.gke_config.name].id
    }
  }
  provider   = google-beta
  depends_on = [module.gke, null_resource.exec_gateway_apis]
}

resource "google_gke_hub_feature_membership" "acm_feature_member" {
  for_each   = { for cluster in local.gke_clusters : cluster.cluster_num => cluster }
  location   = "global"
  feature    = google_gke_hub_feature.acm_feature.name
  membership = google_gke_hub_membership.membership[each.key].membership_id
  configmanagement {
    version = "1.9.0"
    config_sync {
      git {
        sync_repo = "https://github.com/hashicorp/terraform"
      }
    }
  }
  provider   = google-beta
  depends_on = [module.gke]
}

