module "fleet" {
  source       = "../../../../modules/gcp/fleet/"
  project_id   = var.project_id
  network_name = var.network_name
  fleets       = var.fleets
}

