output "network" { value = module.fleet.network }
output "project_id" { value = module.fleet.project_id }
output "network_name" { value = module.fleet.network_name }
output "subnets" { value = module.fleet.subnets }
output "subnets_ips" { value = module.fleet.subnets_ips }
output "subnets_names" { value = module.fleet.subnets_names }
output "subnets_regions" { value = module.fleet.subnets_regions }
output "subnets_secondary_ranges" { value = module.fleet.subnets_secondary_ranges }
output "clusters" {
  value     = module.fleet.clusters
  sensitive = true
}
